
include_recipe "ci"
include_recipe "nginx::repo"
include_recipe "nginx"

#service 'nginx' do
#  action :start
#end

case deploy_env
when 'ci'
  env='dev'
else
  env=deploy_env
end


template '/etc/nginx/conf.d/default.conf' do
  source 'nginx.conf'
  mode '644'
  notifies :restart, 'service[nginx]', :delayed
end


deploy_tarball "edu-ui.tgz" do
  release_dir "/var/opt/edu-ui"
  app_user "nginx"
  app_group "nginx"
  version node["project_info"]["pinpromote"]["version"]
  project "edu-ui"
  target_dir "/usr/share/nginx/html/apps/edu"
  source  "archive"
end

template "/usr/share/nginx/html/apps/edu/dist/js/edu-env.js" do
  source "edu-env.js.erb"
  owner "nginx"
  mode "0444"
  variables(
    :deploy_env => env
  )
end
